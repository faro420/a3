Projekt:	a3, TI1-P1 SS14
Organisation:	HAW-Hamburg, Informatik
Team:		S1T5
Autor(en):
Baha, Mir Farshid	(mirfarshid.baha@haw-hamburg.de)
G�tze, Nico		(nico.goetze@haw-hamburg.de)

Review History:
===============
140408 v1.0 failed Schafers unhappy,ein if zu viel, tabs beseitigen
140411 v1.1 removed tabs, removed unneeded ifs, restricted input to 1<=input<=47
140422 v2.0 ok by Schafers