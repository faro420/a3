package a3;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Nico, G�tze> (<2125118>,<nico.goetze@haw-hamburg.de>),
 */
public class Fibonacci {
    public static void main(String args[]) {
        final int input = 47;

        int n = input;
        int fib0 = 0;
        int fib1 = 1;
        int fib2 = 1;
        if (n < 1||n>47) {
            System.out.print("invalid input!");
        } else if (n == 1) {
            System.out.print("0");
        } else if (n >= 2) {
            System.out.print("0,1");
            for (int i = 2; i < n; i++) {
                System.out.print(",");
                fib2 = fib0 + fib1;
                fib0 = fib1;
                fib1 = fib2;
                System.out.print(fib2);
            }
        }
    }
}
